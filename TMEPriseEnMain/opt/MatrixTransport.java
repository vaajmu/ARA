package helloWorld;

import peersim.config.*;
import peersim.core.*;
import peersim.edsim.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MatrixTransport implements Protocol {

    // Fichier contenant la matrice de latences
    private final String matrixFileName;
    private static long matrix[][];
    
    
    public MatrixTransport(String prefix) {
	System.out.println("Transport Layer Enabled : MatrixTransport");
	matrixFileName = Configuration.getString(prefix + ".matrixfile");
	try{
	    BufferedReader reader = new BufferedReader(new FileReader(matrixFileName));
	    String line = null;
	    List<String> items = new ArrayList<String>();
	    StringTokenizer split;
	    while ((line = reader.readLine()) != null) {
		items.add(line);
	    }
	    int size = items.size();
	    System.out.println("MatrixTransport : " + size + "x" + size + " network detected");
	    matrix = new long[size][size];
            System.out.println("Initialisation de la matrice :");
	    for (int i=0; i<size; i++) {
		split = new StringTokenizer(items.get(i), " ");		
		for(int j=0; j<size; j++) {	
		    matrix[i][j] = Long.parseLong((String)split.nextElement());
                    // Si le destinataire est pair, on double la latence
                    // System.out.println("Avant appel :");
                    // System.out.println(Network.get(i));
                    // System.out.println("Après appel");

                    // if(j%2==0) setLatency(Network.get(i), Network.get(j),
                    //                       2*matrix[i][j]);
                    // else setLatency(Network.get(i),
                    //                 Network.get(j), matrix[i][j]);
                    // System.out.print(new Long(getLatency(Network.get(i),
                    //                                      Network.get(j))).toString()
                    //                  + " ");
		}
                // System.out.print("\n");
	    }
            // System.out.print("----------------------------\n");
	} catch(IOException e) {
            System.out.println("Erreur MatrixTransport init");
            e.printStackTrace();
	}
    }
    
    public Object clone() {
	return this;
    }
    

    //envoi d'un message: il suffit de l'ajouter a la file d'evenements
    public void send(Node src, Node dest, Object msg, int pid) {
	long delay = getLatency(src,dest);
	EDSimulator.add(delay, msg, dest, pid);
    }
    
    
    //latence random entre la borne min et la borne max
    public long getLatency(Node src, Node dest) {
        System.out.println("    Appel getLatency : " + src.getID() + ", " + dest.getID());
        if(dest.getID()%2==0)
            setLatency(src, dest, 2 * matrix[(int)src.getID()][(int)dest.getID()]);
        System.out.println("    retourne " + matrix[(int)src.getID()][(int)dest.getID()]);
	return matrix[(int)src.getID()][(int)dest.getID()];
    }

    public long setLatency(Node src, Node dest, long newLatency) {
        System.out.println("      Appel setLatency : " + src.getID() + ", " +
                           dest.getID() + ", " + newLatency);
	long oldLatency = matrix[(int)src.getID()][(int)dest.getID()];
	matrix[(int)src.getID()][(int)dest.getID()] = newLatency;
	return oldLatency;
    }
}

