package helloWorld;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class HelloWorld implements EDProtocol {
    
    // Numéro du tour courant
    private int round = 0;

    //identifiant de la couche transport
    private int transportPid;

    //objet couche transport
    private MatrixTransport transport;

    //identifiant de la couche courante (la couche applicative)
    private int mypid;

    //le numero de noeud
    private int nodeId;

    //prefixe de la couche (nom de la variable de protocole du fichier de config)
    private String prefix;

    // état local
    private long state;

    // Nombre de round avant de se terminer
    private int nbRoundsToDo;

    // Probabilités d'envoyer un message aux autres noeuds
    private int unite;
    private int proba1;
    private int proba2;

    public HelloWorld(String prefix) {
	this.prefix = prefix;
	//initialisation des identifiants a partir du fichier de configuration
	this.transportPid = Configuration.getPid(prefix + ".transport");
	this.mypid = Configuration.getPid(prefix + ".myself");
	this.transport = null;
	this.nbRoundsToDo = Configuration.getInt(prefix + ".nbRounds");
        this.unite = Configuration.getInt(prefix + ".uniteProba");
        this.proba1 = Configuration.getInt(prefix + ".probaEnvoiEvtLocal");
        this.proba2 = Configuration.getInt(prefix + ".probaBCastEvtLocal");
    }

    //methode appelee lorsqu'un message est recu par le protocole HelloWorld du noeud
    public void processEvent( Node node, int pid, Object event ) {
        // System.out.println("Message from " + node.getID() + " to " + nodeId);
        Message msg = (Message) event;

        switch(msg.getType()) {
        case(Message.LOCAL_EVENT):
            processLocalEvent(msg);
            break;
        case(Message.STOP):
            System.out.println("KILLING_MYSELF;" + nodeId
                               + ";" + CommonState.getTime());
            Network.get(nodeId).setFailState(Fallible.DEAD);
            break;
        case(Message.HELLOWORLD):
            deliver(msg);
            break;
        default:
            System.err.println("Erreur switch message type : unknown");
        }
    }

    private void processStopEvent(Message m) {
        Network.get(nodeId).setFailState(Fallible.DEAD);
    }

    private void processLocalEvent(Message m) {
        // On incrémente notre état et on prévoit le prochain évènment local
        state++;
        System.out.println("LOCAL_EVENT;" + nodeId + ";" + 
                           CommonState.getTime() + ";" + state);
        Message nextLocalEvt = new Message(nodeId, nodeId, Message.LOCAL_EVENT, "");
        EDSimulator.add(10 + CommonState.r.nextLong(10),
                        nextLocalEvt,
                        Network.get(this.nodeId),
                        mypid);

        // On envoie les msg si nécessaire
        if(CommonState.r.nextLong(unite) < proba1) {
            // probabilité de 0.5 d'envoyer à un autre processus
            // On choisit un noeud aléatoirement
            int idNd = (int) CommonState.r.nextLong(Network.size());
            Message msg1 = new Message(nodeId, idNd, Message.HELLOWORLD, "Hi");
            this.send(msg1);
        } if(CommonState.r.nextLong(unite) < proba2) {
            // probabilité de 0.005 de broadcast
            Message msg2 = new Message(nodeId, -1, Message.HELLOWORLD, "Hi all");
            this.broadcast(msg2);
        }
    }
    
    //methode necessaire pour la creation du reseau (qui se fait par clonage d'un prototype)
    public Object clone() {

	HelloWorld dolly = new HelloWorld(this.prefix);

	return dolly;
    }

    //liaison entre un objet de la couche applicative et un 
    //objet de la couche transport situes sur le meme noeud
    public void setTransportLayer(int nodeId) {
	this.nodeId = nodeId;
	this.transport = (MatrixTransport) Network.get(this.nodeId).getProtocol(this.transportPid);
    }

    public void broadcast(Message m) {
        System.out.println("HELLO_WORLD_BCAST;" + nodeId + ";" +
                           CommonState.getTime());
        for(int i=0; i<Network.size(); i++) {
            Message m2 = m.clone();
            m2.setDestinataire(i);
            this.send(m2);
        }
    }

    //envoi d'un message (l'envoi se fait via la couche transport)
    public void send(Message msg) {
        // Le paramètre dest ne sert plus à rien, le destinataire est spécifié à
        // la construction du message et l'envoi se fait à son voisin puis-que
        // c'est un annau
        System.out.println("HELLO_WORLD_SEND;" + nodeId + ";" + CommonState.getTime()
                           + ";" + msg.getDestinataire());
	this.transport.send(getMyNode(), Network.get((int)msg.getDestinataire()),
                            msg, this.mypid);
    }

    private int getNextId() {
        return (nodeId+1)%Network.size();
    }

    //affichage a la reception
    private void deliver(Message msg) {
        System.out.println("HELLO_WORLD_RECV;" + nodeId + ";" + 
                           CommonState.getTime() + ";" + msg.getIdEmitter()
                           + ";" + msg.getContent());
    }

    //retourne le noeud courant
    private Node getMyNode() {
	return Network.get(this.nodeId);
    }

    public String toString() {
	return "Node "+ this.nodeId;
    }

    
}
