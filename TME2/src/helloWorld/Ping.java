package helloWorld;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class Ping implements EDProtocol, IDetector {

    //prefixe de la couche (nom de la variable de protocole du fichier de config)
    private String prefix;

    // le numero de noeud
    private int nodeId;

    // Le PID de la couche Ping
    private int mypid;
    private MatrixTransport transport;
    private int transportPid;

    // Données pour le détecteur de fautes "Ping"
    private int periodePing;
    private int delaiPong;
    private long dateDernierPing;
    private boolean[] failStates;
    private long[] datesDernierPong;

    public Ping(String prefix) {
	this.prefix = prefix;

	//initialisation des identifiants a partir du fichier de configuration
	this.mypid = Configuration.getPid(prefix + ".myself");
	this.transportPid = Configuration.getPid(prefix + ".transport");

        this.periodePing = Configuration.getInt(prefix + ".ping.periodePing");
        this.delaiPong = Configuration.getInt(prefix + ".ping.delaiPong");

        // Les tableaux seront initialisés à faux/0 par défaut
        this.failStates = new boolean[Network.size()];
        this.datesDernierPong = new long[Network.size()];
    }

    @Override
    public void startProtocol(int id) {
        this.nodeId = id;
        this.transport = (MatrixTransport) Network.get(this.nodeId)
            .getProtocol(transportPid);
        sendPing();
    }

    // methode appelee lorsqu'un message est recu par le protocole HelloWorld du noeud
    @Override
    public void processEvent( Node node, int pid, Object event ) {
        if(event instanceof PingMessage);
        else {
            System.err.println("PingMessage : Erreur switch message type");
            return;
        }
        PingMessage msg = (PingMessage) event;

        switch(msg.getType()) {
        case(PingMessage.PING):
            processPing(msg);
            break;
        case(PingMessage.PONG):
            processPong(msg);
            break;
        case(PingMessage.FIN_DELAI_PING):
            processFinDelaiPing(msg);
            break;
        case(PingMessage.FIN_PERIODE_PING):
            processFinPeriodePing(msg);
            break;
        default:
            System.err.println("Ping : Erreur switch message type");
        }
    }

    private void sendPing() {
        // On envoie un ping à tout le monde sauf à soi
        for(int i = 0; i < Network.size(); i++) {
            if(i == nodeId) continue;
            this.transport.send(Network.get(this.nodeId), Network.get(i), 
                                new PingMessage(this.nodeId, i, 
                                                PingMessage.PING), this.mypid);
            System.out.println("PING_SEND_PING;" + this.nodeId + ";" +
                               CommonState.getTime() + ";" + i);
        }
        // S'envoyer un message pour notifier la fin du délai de réponse
        EDSimulator.add(delaiPong,
                        new PingMessage(this.nodeId, this.nodeId, 
                                        PingMessage.FIN_DELAI_PING),
                        Network.get(this.nodeId), mypid);

        // S'envoyer un message pour notifier la fin d'une periode
        EDSimulator.add(periodePing,
                        new PingMessage(this.nodeId, this.nodeId, 
                                        PingMessage.FIN_PERIODE_PING),
                        Network.get(this.nodeId), mypid);
    }

    private void processFinDelaiPing(PingMessage m) {
        // Si un processusn'a pas envoyé de pong depuis plus de delaiPong, on le
        // déclare mort
        System.out.println("PING_RECV_FIN_DELAI_PING;" + this.nodeId + ";" +
                           CommonState.getTime());
        for(int i=0; i<Network.size(); i++) {
            if(i == nodeId) continue;
            // On vérifie d'abord s'il n'était pas déjà détecté pour ne pas
            // afficher plusieurs fois la détection et ensuite on regarde son
            // dernier sige de vie
            if(failStates[i] == false && 
               datesDernierPong[i] + delaiPong < CommonState.getTime()) {
                failStates[i] = true;
                System.out.println("FAILURE_DETECTION;" + this.nodeId + ";" + 
                                   CommonState.getTime() + ";" + i);
            }
        }
    }

    private void processFinPeriodePing(PingMessage m) {
        System.out.println("PING_RECV_FIN_PERIODE_PING;" + this.nodeId + ";" +
                           CommonState.getTime());
        // Quand la periode du ping précédent se termine, on renvoie des pings
        sendPing();
    }

    private void processPong(PingMessage m) {
        // Si jamais le pong correspond à un autre ping, on considère quand même
        // qu'il est vivant, si le lien de comminucation en questio est très
        // lent et que le délai est mal géré, le processus serait constemment
        // considéré comme fautif. Ici, la détection peut être plus longue dans
        // ce cas, mais le processus participera quand même le reste du temps.

        System.out.println("PING_RECV_PONG;" + this.nodeId + ";" +
                           CommonState.getTime() + ";" + m.getIdEmitter());
        // On prends en compte le dernier signe de vie d'un processus, si ce
        // processus était considéré mort, on rectifie
        datesDernierPong[(int)m.getIdEmitter()] = CommonState.getTime();
        if(failStates[(int)m.getIdEmitter()] == true) {
            failStates[(int)m.getIdEmitter()] = false;
            // Transparence complète, on publie l'erreur
            System.out.println("FALSE_DETECTION;" + this.nodeId + ";" + 
                               CommonState.getTime() + ";" + m.getIdEmitter());
        }
    }

    private void processPing(PingMessage m) {
        System.out.println("PING_RECV_PING;" + this.nodeId + ";" +
                           CommonState.getTime() + ";" + m.getIdEmitter());
        // On renvoit la balle
        this.transport.send(
                            Network.get(this.nodeId),
                            Network.get((int)m.getIdEmitter()),
                            new PingMessage(this.nodeId, m.getIdEmitter(),
                                            PingMessage.PONG),
                            this.mypid);
    }

    //methode necessaire pour la creation du reseau (qui se fait par clonage d'un prototype)
    public Object clone() {

	Ping dolly = new Ping(this.prefix);

	return dolly;
    }

    public String toString() {
	return "Node "+ this.nodeId;
    }
    
}
