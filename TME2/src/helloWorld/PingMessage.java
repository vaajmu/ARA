package helloWorld;

import peersim.edsim.*;

public class PingMessage {

    public final static int PING = 1;
    public final static int PONG = 2;
    public final static int FIN_DELAI_PING = 3;
    public final static int FIN_PERIODE_PING = 4;

    private int type;
    private long idEmitter;
    private long destinataire;

    public PingMessage(long idEmitter, long dest, int type) {
	this.type = type;
	this.idEmitter = idEmitter;
	this.destinataire = dest;
    }

    public int getType() {
	return this.type;
    }

    public long getDestinataire() {
        return this.destinataire;
    }

    public long getIdEmitter() {
	return this.idEmitter;
    }
    
    public PingMessage clone() {
        return new PingMessage(idEmitter, destinataire, type);
    }

}
