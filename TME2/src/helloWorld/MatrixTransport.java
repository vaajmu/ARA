package helloWorld;

import peersim.config.*;
import peersim.core.*;
import peersim.edsim.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MatrixTransport implements Protocol {

    // Fichier contenant la matrice de latences
    private final String matrixFileName;
    private static long matrix[][][];

    public MatrixTransport(String prefix) {
	System.out.println("Transport Layer Enabled : MatrixTransport");
	matrixFileName = Configuration.getString(prefix + ".matrixfile");
	try{
	    BufferedReader reader = new BufferedReader(new FileReader(matrixFileName));
	    String line = null;
	    List<String> items = new ArrayList<String>();
	    StringTokenizer split, split2;
	    while ((line = reader.readLine()) != null) {
		items.add(line);
	    }
	    int size = items.size();
	    System.out.println("MatrixTransport : " + size + "x" + size + " network detected");
	    matrix = new long[size][size][2];
            System.out.println("Initialisation de la matrice :");
	    for (int i=0; i<size; i++) {
		split = new StringTokenizer(items.get(i), " ");		
		for(int j=0; j<size; j++) {	
                    String minEtMax = (String)split.nextElement();
                    split2 = new StringTokenizer(minEtMax, ",");		
		    matrix[i][j][0] = Long.parseLong((String)split2.nextElement());
		    matrix[i][j][1] = Long.parseLong((String)split2.nextElement());
		}
	    }
	} catch(FileNotFoundException e) {
            System.out.println("Erreur MatrixTransport init : le fichier "
                               + matrixFileName
                               + "n'existe pas");
            e.printStackTrace();
	} catch(IOException e) {
            System.out.println("Erreur MatrixTransport init");
            e.printStackTrace();
	}
    }
    
    public Object clone() {
	return this;
    }
    

    //envoi d'un message: il suffit de l'ajouter a la file d'evenements
    public void send(Node src, Node dest, Object msg, int pid) {
        // Déterminer le temps d'acheminement du message
	long delay = getLatency(src,dest);

        // Envoi
	EDSimulator.add(delay, msg, dest, pid);
    }
    
    
    //latence random entre la borne min et la borne max
    public long getLatency(Node src, Node dest) {
        long lat;
        long minLat, maxLat;
        // Retourner une valeur entre le min et le max : min + alea(0, max)
        minLat = matrix[(int)src.getID()][(int)dest.getID()][0];
        maxLat = matrix[(int)src.getID()][(int)dest.getID()][1];

        // On suppose ici que les latences sont positives oou nulles et que le
        // min est inférieur ou égal au max. Si les deux sont égales, la latence
        // est fixe !
        if(maxLat - minLat > 0) 
            lat = minLat + CommonState.r.nextLong(maxLat - minLat);
        else
            lat = minLat;


	if(CommonState.getTime() < 200 && (src.getID() + dest.getID())%3 == 0) {
		// On ajoute 20% de la latence au début de la simulation sur
		// certains liens. Les liens en question sont choisi
		// arbitrairement par le modulo de l'addition des deux
		// identifiants.
		lat = lat * 120 / 100;
	}

        return lat;
    }

    public long[] setLatency(Node src, Node dest, long newMinLatency, long newMaxLatency) {
	long oldLatency[] = new long[2];
        oldLatency[0] = matrix[(int)src.getID()][(int)dest.getID()][0];
        oldLatency[1] = matrix[(int)src.getID()][(int)dest.getID()][1];
	matrix[(int)src.getID()][(int)dest.getID()][0] = newMinLatency;
	matrix[(int)src.getID()][(int)dest.getID()][1] = newMaxLatency;
	return oldLatency;
    }
}

