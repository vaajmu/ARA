package helloWorld;

import peersim.edsim.*;

public class HBMessage {

    // Dans le cas d'un évènement pour signaler la fin du délai (evt interne),
    // l'emmeteur est le processus correspondant au temporisateur

    // Le message basique à envoyer aux autres
    public final static int HB = 1;
    // Temps au bout duquel il faut renvoyer un hb
    public final static int FIN_PERIODE_HB = 2;
    // Temps au bout duquel on devrait avoir reçu le hb
    public final static int FIN_DELAI_HB = 3;

    private int type;
    private long idEmitter;
    private long destinataire;

    public HBMessage(long idEmitter, long dest, int type) {
	this.type = type;
	this.idEmitter = idEmitter;
	this.destinataire = dest;
    }

    public int getType() {
	return this.type;
    }

    public long getDestinataire() {
        return this.destinataire;
    }

    public long getIdEmitter() {
	return this.idEmitter;
    }
    
    public HBMessage clone() {
        return new HBMessage(idEmitter, destinataire, type);
    }

}
