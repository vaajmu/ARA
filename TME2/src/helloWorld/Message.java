package helloWorld;

import peersim.edsim.*;

public class Message {

    public final static int HELLOWORLD = 0;
    public final static int LOCAL_EVENT = 1;
    public final static int STOP = 2;

    private int type;
    private String content;
    private long idEmitter = -1;
    private long destinataire;

    Message(long idEmitter, long dest, int type, String content) {
	this.type = type;
	this.content = content;
	this.idEmitter = idEmitter;
	this.destinataire = dest;
    }

    public String getContent() {
	return this.content;
    }

    public int getType() {
	return this.type;
    }

    public void setDestinataire(long d) {
        this.destinataire = d;
    }

    public long getDestinataire() {
        return this.destinataire;
    }

    public long getIdEmitter() {
	return this.idEmitter;
    }
    
    public Message clone() {
        return new Message(idEmitter, destinataire, type, content);
    }

}
