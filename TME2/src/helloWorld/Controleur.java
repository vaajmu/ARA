package helloWorld;

import java.util.List;
import java.util.ArrayList;


import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class Controleur implements Control {

    private int probaPanneFranche;
    private int pas;
    private int appli;

    public Controleur (String prefix) {
        probaPanneFranche = 
            Configuration.getInt(prefix + ".probaPanneFranche");

        pas = Configuration.getInt(prefix + ".step");

        appli = Configuration.getPid(prefix + ".applicative");
    }
        
    public boolean execute() {
        int delta;              // Temps avant de faire tomber le noeud
        long noeud;              // Le noeud à faire tomber
        int alea = (int) CommonState.r.nextLong(10);

        // System.out.println("CONTROLEUR_EXECUTE;" + CommonState.getTime() + ";" + alea);

        // Si l'alétoire est sup à la proba, on ne fait pas tomber de noeud
        if(alea >= probaPanneFranche) return false;

        // Différer la panne
        delta = (int) CommonState.r.nextLong(pas);

        // Pour un aléatoire uniforme sur la sélection du noeud à faire tomber,
        // on créé une liste avec tous les noeuds actifs et on tire ensuite un
        // nombre aéatoire entre 0 et le nombre de noeuds. Cela nous permet
        // d'avoir la liste et le nombre de noeuds qui sont actifs.
        List<Node> liste = new ArrayList<Node>();
        for(int i = 0; i<Network.size(); i++) {
            Node cur = Network.get(i);
            if(cur.getFailState() != Fallible.DEAD)
                liste.add(cur);
        }
        // On arrête là si aucun noeud n'est actif
        if(liste.size() == 0) return true;

        Node victime = liste.get((int)CommonState.r.nextLong(liste.size()));
        EDSimulator.add(delta, new Message(-1, victime.getID(),
                                           Message.STOP, ""), 
                        victime, appli);

        System.out.println("CONTROLEUR_KILL;" + CommonState.getTime() + ";"
                           + victime.getID() + ";" + (CommonState.getTime()+delta));


        return false;
    }

}

