package helloWorld;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

/*
  Module d'initialisation de helloWorld: 
  Fonctionnement:
  pour chaque noeud, le module fait le lien entre la couche transport et la couche applicative
  ensuite, il fait envoyer au noeud 0 un message "Hello" a tous les autres noeuds
*/
public class Initializer implements peersim.core.Control {

    private String prefix;
    
    public Initializer(String prefix) {
        this.prefix = prefix;
    }

    public boolean execute() {
	int nodeNb, detectorPid;
	HelloWorld current;
        IDetector detecteur;
	Message msg1, msg2;
        Node tmp;
        int helloWorldPid;

	//recuperation du pid de la couche applicative
	helloWorldPid = Configuration.getPid(prefix + ".helloWorldProtocolPid");

        // Récupérer le pid du détecteur
	detectorPid = Configuration.getPid(prefix + ".detectorPid");

	//recuperation de la taille du reseau
	nodeNb = Network.size();
        System.out.println("NETWORK_SIZE;" + nodeNb);
	//creation du message
	if (nodeNb < 1) {
	    System.err.println("Network size is not positive");
	    System.exit(1);
	}

        // On définit la couche transport, la couche ping et on prévoit l'evt
        // local de caque noeud
        for(int i=0; i<nodeNb; i++) {
            tmp = Network.get(i);

            current = (HelloWorld)tmp.getProtocol(helloWorldPid);
            current.setTransportLayer(i);

            detecteur = (IDetector) tmp.getProtocol(detectorPid);
            detecteur.startProtocol(i);

            EDSimulator.add(10 + CommonState.r.nextLong(20),
                            new Message(-1, i, Message.LOCAL_EVENT, ""),
                            tmp,
                            helloWorldPid);

        }

        System.out.println("Initialization completed");
	return false;
    }
}
