package helloWorld;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class Heartbeat implements EDProtocol, IDetector {

    //prefixe de la couche (nom de la variable de protocole du fichier de config)
    private String prefix;

    // le numero de noeud
    private int nodeId;

    // Le PID de la couche Ping
    private int mypid;
    private MatrixTransport transport;
    private int transportPid;

    // Données pour le détecteur de fautes "Heartbeat"
    // Temps au bout duquel renvoyer un hb
    private int periodeHB;
    // Temps au bout duquel on devrait avoir reçu un hb
    private int delaiHB;

    private boolean[] failStates;


    // Choix des stratégies
    private int[] strategie;

    private long[] datesDernierHearbeat;

    // Str avg
    private long[] sommeDelaisHB;
    private int[] nbHB;

    // Str max
    private int[] max;

    // Les différentes stratégies
    private final int LAST_STR = 1;
    private final int AVG_STR = 2;
    private final int MAX_STR = 3;

    public Heartbeat(String prefix) {
	this.prefix = prefix;

	//initialisation des identifiants a partir du fichier de configuration
	this.mypid = Configuration.getPid(prefix + ".myself");
	this.transportPid = Configuration.getPid(prefix + ".transport");

        this.periodeHB = Configuration.getInt(prefix + ".hb.periodeHB");
        this.delaiHB = Configuration.getInt(prefix + ".hb.delaiHB");

        // Les tableaux seront initialisés à faux/0 par défaut
        this.failStates = new boolean[Network.size()];
        this.datesDernierHearbeat = new long[Network.size()];
        this.sommeDelaisHB = new long[Network.size()];
        this.nbHB = new int[Network.size()];
        this.strategie = new int[Network.size()];
        this.max = new int[Network.size()];

	for(int i = 0; i < Network.size(); i++) {
	    strategie[i] = MAX_STR;
	}
    }

    @Override
    public void startProtocol(int id) {
        this.nodeId = id;
        this.transport = (MatrixTransport) Network.get(this.nodeId)
            .getProtocol(transportPid);
        sendHB();
    }

    // methode appelee lorsqu'un message est recu par le protocole HelloWorld du noeud
    @Override
    public void processEvent( Node node, int pid, Object event ) {
        if(event instanceof HBMessage);
        else {
            System.err.println("HBMessage : Erreur switch message type");
            return;
        }
        HBMessage msg = (HBMessage) event;

        switch(msg.getType()) {
        case(HBMessage.FIN_PERIODE_HB):
            processFinPeriode(msg);
            break;
        case(HBMessage.FIN_DELAI_HB):
            processFinDelai(msg);
            break;
        case(HBMessage.HB):
            processHB(msg);
            break;
        default:
            System.err.println("HBMessage : Erreur switch message type");
        }
    }

    private void sendHB() {
        // On envoie un hb à tout le monde sauf à soi
        for(int i = 0; i < Network.size(); i++) {
            if(i == nodeId) continue;
            this.transport.send(Network.get(this.nodeId), Network.get(i), 
                                new HBMessage(this.nodeId, i, 
                                                HBMessage.HB), this.mypid);
            System.out.println("HB_SEND_HB;" + this.nodeId + ";" +
                               CommonState.getTime() + ";" + i);
        }

        // S'envoyer un message pour notifier la fin d'une periode
        EDSimulator.add(periodeHB,
                        new HBMessage(this.nodeId, this.nodeId, 
                                        HBMessage.FIN_PERIODE_HB),
                        Network.get(this.nodeId), mypid);
    }

    private void processFinDelai(HBMessage m) {
        System.out.println("HB_RECV_FIN_DELAI_HB;" + this.nodeId + ";" +
                           CommonState.getTime() + ";" + m.getIdEmitter());

        // Si un processusn'a pas envoyé de HB depuis plus de delaiHB, on le
        // déclare mort
	if(datesDernierHearbeat[(int)m.getIdEmitter()] + delaiHB
	   < CommonState.getTime()) {
	    failStates[(int)m.getIdEmitter()] = true;
	    System.out.println("FAILURE_DETECTION;" + this.nodeId + ";" + 
			       CommonState.getTime() + ";" + m.getIdEmitter());
	}
    }

    private void processFinPeriode(HBMessage m) {
        System.out.println("HB_RECV_FIN_PERIODE_HB;" + this.nodeId + ";" +
                           CommonState.getTime());
        // Quand la periode du ping précédent se termine, on renvoie des pings
        sendHB();
    }

    private void processHB(HBMessage m) {
        System.out.println("HB_RECV_HB;" + this.nodeId + ";" +
                           CommonState.getTime() + ";" + m.getIdEmitter());

	if(failStates[(int)m.getIdEmitter()] == true) {
		System.out.println("FALSE_DETECTION;" + this.nodeId + ";" +
				   CommonState.getTime() + ";" +
				   m.getIdEmitter());
	}

	int nextTime = calculerProchainDelai((int)m.getIdEmitter());
	EDSimulator.add(nextTime, new HBMessage((int)m.getIdEmitter(), nodeId, 
						HBMessage.FIN_DELAI_HB),
			Network.get(nodeId), this.mypid);

	datesDernierHearbeat[(int)m.getIdEmitter()] = CommonState.getTime();
    }

    private int calculerProchainDelai(int noeud) {
	int lastTime = (int)CommonState.getTime() - (int)datesDernierHearbeat[noeud];

	switch(strategie[noeud]) {
	case LAST_STR:
	    return lastTime;

	case AVG_STR:
	    sommeDelaisHB[noeud] += lastTime;
	    nbHB[noeud]++;
	    return (int)sommeDelaisHB[noeud] / nbHB[noeud];

	case MAX_STR:
	    if(lastTime > max[noeud])
		max[noeud] = lastTime;
	    return max[noeud];

	default:
	    System.err.println("Heartbeat erreur typ de message inconnu");
	    return -1;
	}
    }

    //methode necessaire pour la creation du reseau (qui se fait par clonage d'un prototype)
    public Object clone() {

	Heartbeat dolly = new Heartbeat(this.prefix);

	return dolly;
    }

    public String toString() {
	return "Node "+ this.nodeId;
    }
    
}
