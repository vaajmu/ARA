Projet ARA :

Antoine LOHÉAC

le lundi 18 janvier 2016


Utilisation :

make
 - Compiler les sources


make run
 - Lancer une simulation


Script genererMatrice nbNoeuds
 - Génère une matrice de latences pour n noeuds


Script launch
 - Compiler et lancer une simulation, sauvegarder les traces dans un fichier,
calculer les stats et les ajouter au fichier et ouvrir ce fichier


Script computeStats
 - Calculer les stats des messages applicatifs


Script computeStatsPing
 - Calculer les stats des messages du protocole PING


Script computeStatsHB
 - Calculer les stats des messages du protocole PING


Script bench
 - Manipule le fichier de configuration et la stratégie du fichier Heartbeat
pour lancer plusieurs simulations. Les simulations sont décrites à la fin du
script. ATTENTION : pour que les simulations s'enchainent, il faut désactiver la
dernière less à la fin du script launch

