#! /bin/bash
# bench.sh

function test() {
# test fichier periode delai detecteur [strategie]

    fichier="$1"
    periode="$2"
    delai="$3"
    detecteur="$4"
    strategie="$5"

    [[ -z "$periode" ]] && echo "erreur args" && exit 1
    [[ -z "$delai" ]] && echo "erreur args" && exit 1
    [[ -z "$detecteur" ]] && echo "erreur args" && exit 1

    [[ "$detecteur" = "Heartbeat" ]] && [[ -z "$strategie" ]] && \
	echo "Quelle str pour Heartbeat ?" && exit 1

    if [ "$detecteur" = "Ping" ]
    then
	domainePourVariable="ping"
	suffixePourVariable="Ping"
	suffixePourDelai="Pong"
    else			# Heartbeat
	domainePourVariable="hb"
	suffixePourVariable="HB"
	suffixePourDelai="HB"
    fi


    # Remplacer le protocole
    match="^(protocol.detector helloWorld.).*$"
    replace="\1""$detecteur"
    sed -ri "s/$match/$replace/" "$fichier"

    # Remplacer la periode
    match="^(protocol.detector.""$domainePourVariable"".periode""$suffixePourVariable"" ).*$"
    replace="\1""$periode"
    sed -ri "s/$match/$replace/" "$fichier"

    # Remplacer le délai
    match="^(protocol.detector.""$domainePourVariable"".delai""$suffixePourDelai"" ).*"
    replace="\1""$delai"
    sed -ri "s/$match/$replace/" "$fichier"

    if [ "$detecteur" = "Heartbeat" ]
    then
	# Remplacer le délai
	match="^(strategie\[i\] = ).*$"
	replace="\1""$strategie"
	sed -ri "s/$match/$replace/" "src/helloWorld/Heartbeat.java"
    fi

    # make
    # echo "coucou"
    fichierCree=$(./launch 2>&1 | sed -nr 's/Fichier créé : (.*)$/\1/p')
    # echo $fichierCree

    echo "Détecteur $detecteur"
    echo "Periode $periode"
    echo "Délai $delai"
    [[ "$detecteur" = "Heartbeat" ]] && echo "Stratégie $strategie"
    tail -n 5 "$fichierCree"
    echo ""
    echo ""
}

# test fichier periode delai detecteur [strategie]

test config_file.cfg 10 14 Ping
test config_file.cfg 10 20 Ping
test config_file.cfg 10 5 Ping

test config_file.cfg 10 14 Heartbeat LAST_STR
test config_file.cfg 10 20 Heartbeat LAST_STR
test config_file.cfg 10 5 Heartbeat LAST_STR

test config_file.cfg 10 14 Heartbeat AVG_STR
test config_file.cfg 10 20 Heartbeat AVG_STR
test config_file.cfg 10 5 Heartbeat AVG_STR

test config_file.cfg 10 14 Heartbeat MAX_STR
test config_file.cfg 10 20 Heartbeat MAX_STR
test config_file.cfg 10 5 Heartbeat MAX_STR







exit 0
