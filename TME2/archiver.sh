#! /bin/bash
# archiver.sh

nomArc=Projet_ARA_LOHEAC
listeFilesToDelete="archiver.sh  bak.config_Q4_1_a_et_b  modifsPourCalendrier.txt  Rapport.odt  Rapport.txt"

tmp=$(mktemp -d)
# dst=$tmp/$nomArc
# mkdir $tmp

ancienDossierRacine=$(basedir $PWD)
curDir=$PWD/$(dirname $0)
arc=$nomArc.tgz

# Supprimer l'ancienne version
echo Suppression de $arc
rm -f $arc

# Copier adns /tmp
echo Copie de $PWD vers $tmp/$nomArc
mkdir $tmp/$nomArc
cp -r . $tmp/$nomArc

echo Déplacement dans "$tmp/$ancienDossierRacine"
cd $tmp/$nomArc

echo Suppréssion de $listeFilesToDelete
rm -rf $listeFilesToDelete

echo Déplacement vers le dossier parent et compression vers $curDir/$arc
cd ..
tar cvzf $curDir/$arc .

exit 0
